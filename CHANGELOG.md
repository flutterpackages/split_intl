# 1.0.0

  * Improved code readability.

  * Removed unnecessary dependencies.

  * Added support for files without basenames (e.g.: `.jsonc` or `.json`) in top level language directories.

# 0.0.3

  * Support for files without a name, like `.arb`, `.json` or `.jsonc`.

# 0.0.2

  * Directory names and file names are now used as prefixes for the localized keys.

# 0.0.1

  * Initial release.
