// Library contents copied from https://gitlab.com/flutterpackages/my_utility/-/tree/v1.5.14/lib/extensions/string.dart

extension MyUtilityExtensionStringCapitalize on String {
  /// Returns a copy of this string having its first letter uppercased and the
  /// rest lowercased.
  ///
  /// ```dart
  /// void foo() {
  ///   print('abcd'.capitalize()); // 'Abcd'
  ///   print(''.capitalize());     // ''
  ///   print('Abcd'.capitalize()); // 'Abcd'
  ///   print('ABCD'.capitalize()); // 'Abcd'
  /// }
  /// ```
  String capitalize() {
    switch (length) {
      case 0:
        return this;
      case 1:
        return toUpperCase();
      default:
        return substring(0, 1).toUpperCase() + substring(1).toLowerCase();
    }
  }
}

extension MyUtilityExtensionStringDecapitalize on String {
  /// Returns a copy of this string having its first letter lowercased.
  ///
  /// This function does not change the case of any letters other than the
  /// first one, as opposed to [MyUtilityExtensionStringCapitalize.capitalize].
  ///
  /// ```dart
  /// void foo() {
  ///   print('abcd'.capitalize()); // 'abcd'
  ///   print(''.capitalize());     // ''
  ///   print('Abcd'.capitalize()); // 'abcd'
  ///   print('ABCD'.capitalize()); // 'aBCD'
  /// }
  /// ```
  String decapitalize() {
    switch (length) {
      case 0:
        return this;
      case 1:
        return toLowerCase();
      default:
        return substring(0, 1).toLowerCase() + substring(1);
    }
  }
}

extension MyUtilityExtensionStringSnakeToCamel on String {
  /// Converts this string from snake-case to camel-case.
  ///
  /// ```dart
  /// void foo() {
  ///   print('abc_def'.snakeToCamel()); // 'abcDef'
  ///   print('ABC_DEF'.snakeToCamel()); // 'abcDef'
  ///   print('ABC'.snakeToCamel());     // 'abc'
  ///   print('abc'.snakeToCamel());     // 'abc'
  /// }
  /// ```
  String snakeToCamel() => snakeToPascal().decapitalize();
}

extension MyUtilityExtensionStringSnakeToPascal on String {
  /// Converts this string from snake-case to pascal-case.
  ///
  /// ```dart
  /// void foo() {
  ///   print('abc_def'.snakeToPascal()); // 'AbcDef'
  ///   print('ABC_DEF'.snakeToPascal()); // 'AbcDef'
  ///   print('ABC'.snakeToPascal());     // 'Abc'
  ///   print('abc'.snakeToPascal());     // 'Abc'
  /// }
  /// ```
  String snakeToPascal() {
    // See https://stackoverflow.com/a/1176023
    return split("_").map((word) => word.capitalize()).join();
  }
}
