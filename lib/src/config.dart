part of "../split_intl.dart";

class IntlConfig {
  const IntlConfig({
    required this.arbDir,
    required this.templateArbFile,
  });

  final String arbDir;
  final String templateArbFile;

  List<Object?> get props => [arbDir, templateArbFile];

  /// Get only the language code from [templateArbFile].
  ///
  /// If [templateArbFile] is `"app_en.arb"`, then this
  /// getter returns `"en"`.
  String get templateArbFileLangCode {
    final nameParts = path.basenameWithoutExtension(templateArbFile).split("_");
    return nameParts.last;
  }

  /// Get only the name of [templateArbFile], without
  /// the language code.
  ///
  /// If [templateArbFile] is `"app_en.arb"`, then this
  /// getter returns `"app_"`.
  String get templateArbFileWithoutLangCode {
    final lastUnderscoreIdx = templateArbFile.lastIndexOf("_");
    return templateArbFile.substring(0, lastUnderscoreIdx + 1);
  }

  String getArbFilepath(String languageCode) {
    final filename = "$templateArbFileWithoutLangCode$languageCode.arb";
    return path.join(arbDir, filename);
  }
}

/// Read the projects [localization configurations](https://docs.flutter.dev/accessibility-and-localization/internationalization#configuring-the-l10nyaml-file).
IntlConfig readConfig() {
  // Configurations are always stored in a file called "l10n.yaml" in
  // the current project directory.
  // See:
  // https://docs.flutter.dev/accessibility-and-localization/internationalization#configuring-the-l10nyaml-file
  const configFilepath = "l10n.yaml";

  final yaml = loadYaml(File(configFilepath).readAsStringSync()) as YamlMap;

  return IntlConfig(
    arbDir: yaml["arb-dir"],
    templateArbFile: yaml["template-arb-file"],
  );
}
