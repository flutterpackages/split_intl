import "dart:convert";
import "dart:io";

import "package:jsonc/jsonc.dart";
import "package:path/path.dart" as path;
import "package:split_intl/my_utility.dart";
import "package:split_intl/split_intl.dart";

const encoder = JsonEncoder.withIndent("  ");
const allowedFileExtensions = [".arb", ".json", ".jsonc"];

void main() {
  final config = readConfig();

  final subdirs = Directory(config.arbDir)
      .listSync()
      .where((e) => FileSystemEntityType.directory == e.statSync().type)
      .cast<Directory>()
      .toList(growable: false);

  // Check if subdirs actually contains a dir for language code defined by
  // config.templateArbFile
  final containsDirForTemplateArbFile = subdirs
      .map((e) => path.basename(e.path))
      .any((e) => e == config.templateArbFileLangCode);

  if (!containsDirForTemplateArbFile) {
    final subdirsStr = subdirs.map((e) => "'${e.path}'").join(", ");
    stderr.writeln(
      "There is no directory that can be used to generate the "
      "template arb file.\n"
      "template arb filename: '${config.templateArbFile}'\n"
      "directories: $subdirsStr",
    );
    exit(1);
  }

  // contatenate the files of each languagedir into a single one.
  //
  // Example:
  // assets/localization/de/... -> assets/localization/app_de.arb
  // assets/localization/en/... -> assets/localization/app_en.arb
  for (final langdir in subdirs) {
    final arbMap = <String, Object?>{};

    for (final file in langdir.listSync(recursive: true)) {
      if (file is! File) {
        // We recurse into directories, therefore we do not tell the user that
        // the file system entity is skipped.
        if (file is! Directory) {
          print("Skipping '${file.path}', because it is not a file.\n");
        }
        continue;
      }

      var ext = path.extension(file.path);

      // The path library interprets files without a name like ".json"
      // as if they do not have an extension, but their basename is ".json".
      final hasExtension = ext.isNotEmpty;

      if (!hasExtension) {
        ext = path.basename(file.path);
      }

      if (!allowedFileExtensions.contains(ext)) {
        print(
          "Skipping '${file.path}', because it has an invalid extension. "
          "The only allowed extensions are: "
          "${allowedFileExtensions.join(", ")}\n",
        );
        continue;
      }

      print("Parsing '${file.path}'...");
      final json = jsoncDecode(file.readAsStringSync()) as Map<String, Object?>;

      // Get relative filepath form arbDir/LANG_CODE
      var relFilepath = path.relative(file.path, from: langdir.path);
      print("Relative filepath from '${langdir.path}': $relFilepath");

      relFilepath = hasExtension
          ? path.withoutExtension(relFilepath)
          : path.dirname(relFilepath);

      // Docs for 'path.dirname' say:
      //
      // If a relative path has no directories, then '.' is returned.
      //
      //     p.dirname('foo');  // -> '.'
      //     p.dirname('');  // -> '.'
      //
      // And because the prefix is computed by concatenating the names of
      // all parent directories, we have no prefix in this case (empty string).
      var prefix = "." == relFilepath
          ? ""
          : relFilepath
              .split(path.separator)
              .map((e) => e.snakeToCamel())
              .join("_");

      if (prefix.isEmpty) {
        print("Using no prefix");
      } else {
        prefix = "${prefix}_";
        print("Using prefix: $prefix");
      }

      for (final e in json.entries) {
        final newKey = e.key.startsWith("@")
            ? "@$prefix${e.key.substring(1)}"
            : "$prefix${e.key}";

        arbMap[newKey] = e.value;
      }

      print("");
    }

    // Create an arb file containing the contents of all files
    // from dir.
    final langCode = path.basename(langdir.path);
    File(config.getArbFilepath(langCode)).writeAsStringSync(
      encoder.convert(arbMap),
      mode: FileMode.writeOnly,
    );
  }
}
